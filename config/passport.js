var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    BasicStrategy = require('passport-http').BasicStrategy,
    bcrypt = require('bcryptjs');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findOne({ id: id }, (err, user) => {
        done(err, user);
    });
});

var passportFindUser = function (email, password, done) {
    User.findOne({ email: email }, (err, user) => {
        if (err) { return done(err); }
        if (!user) {
            return done(null, false, { message: 'Incorrect email.' });
        }

        bcrypt.compare(password, user.password, (err, res) => {
            if (!res) {
                return done(null, false, {
                    message: 'Invalid Password'
                });
            }
            var returnUser = {
                email: user.email,
                createdAt: user.createdAt,
                id: user.id
            };
            return done(null, returnUser, {
                message: 'Logged In Successfully'
            });
        });
    });
}


passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(email, password, done) {
        passportFindUser(email, password, done)
    }
));

passport.use(new BasicStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(email, password, done) {
        passportFindUser(email, password, done)
    }
));