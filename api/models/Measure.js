/**""
 * Measure.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var timeoutHandle = function (sensor) {
  sails.log('entering Timeout')
  var timeAgo = new Date();
  timeAgo.setTime(timeAgo.getTime() - sensor.timeout);
  Sensor.findOne({ id: sensor.id })
    .exec(function (error, sensor) {
      if (error) {
        return sails.log(error);
      }
      lastMeasureTime = new Date(sensor.lastMeasure.createdAt);
      //sails.log('currentTime = ' + new Date().toLocaleTimeString());
      //sails.log('timeout = ' + timeAgo.toLocaleTimeString());
      //sails.log('LastMeasure = ' + lastMeasureTime.toLocaleTimeString());
      //sails.log('timeout = ' + timeAgo.getTime());
      //sails.log('LastMeasure = ' + lastMeasureTime.getTime());
      if (lastMeasureTime.getTime() < timeAgo.getTime()) {
        sensor.alert = true;
        sails.log('ALERT on sensor ' + sensor.name + " id: " + sensor.id);
        //Broadcast Message Alert
        if (Sensor) {
          //Sensor.publishUpdate(sensor.id, {alert: true});
          sensor.sendAlert();
        }
        sensor.save();
      }
    })
}

module.exports = {

  attributes: {
    value: {
      type: 'float'
    },
    unit: {
      model: 'unit'
      /*
      type: 'integer',
      foreignKey: true,
      references: 'unit',
      on: 'id',
      via: 'observation'
      */
    },
    sensor: {
      model: 'sensor'
    }
  },

  afterCreate: function (values, cb) {
    Sensor.findOne({ id: values.sensor })
      .exec(function (error, sensor) {
        if (error) {
          sails.log('error on lastValue');
          cb(error);
          return sails.log(error);
        }
        if (!sensor) {
          sails.log('Sensor not found to update lastValue');
          return cb(error);

        }

        sensor.lastMeasure = values;
        if (sensor.alert === true) {
          Incident.create({
            sensor: sensor,
            type: 'start'
          })
          sensor.alert = false;
          sensor.save(function (err) {
            if (err) {
              sails.log('error saving sensor after alert update', err);
            }
            Thing.checkAlert(sensor.thing);
          });
          Sensor.publishUpdate(sensor.id, { alert: false })
          //Check if thing is in alert

        } else {
          sensor.save();
        }


        AlertService.registerTimeout({ sensor: sensor });


        return cb();
      });
  }
};

