/**
 * Sensor.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

let rx = require('rxjs');

module.exports = {

  autosubscribe: ['create', 'destroy', 'update', 'add:measures', 'message'],

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    alert: {
      type: 'boolean',
      required: true,
      defaultsTo: false
    },
    thing: {
      model: 'thing'
    },
    measures: {
      collection: 'measure',
      via: 'sensor'
    },
    lastMeasure: {
      type: 'json',
      defaultsTo: {}
    },
    incidents: {
      collection: 'incident',
      via: 'sensor'
    },
    type: {
      //This attribute is asociated with a hardware
      type: 'string',
      required: true
    },
    timeout: {
      type: 'integer',
      defaultsTo: (5 * 60 * 1000)
    },
    toJSON: function () {
      var obj = this.toObject();
      delete obj.measures;
      return obj;
    },
    sendAlert: function () {
      this.alert = true;
      var title = "Sensor #" + this.id;
      var sensor = this;
      Thing.findOne(this.thing).exec(function (err, thing) {
        if (err) {
          return sails.log('error getting the thing in the notification', err)
        }
        //Propagate alarm to thing
        if (thing.alert === false) {
          thing.alert = true;

          thing.save();
          Thing.publishUpdate(thing.id, { alert: true });
        }

        var group = thing.group;

        var url = "/sensor/" + sensor.id;
        var since = Math.round((sensor.timeout / 1000) * 100) / 100
        var body = `tipo: ${sensor.type}
nombre: ${sensor.name}
Ha dejado de enviar datos desde hace ${since} segundos
`
        Notification.create({
          title: title,
          body: body,
          group: group,
          url: url
        }).exec(function (err, notification) {
          if (err) {
            return sails.log(err);
          }
          PushService.sendNotification(notification);
        })
      })

    }

  },



};

