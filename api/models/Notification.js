/**
 * Notification.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    tag: {
      type: 'string'
    },
    body: {
      type: 'string'
    },
    title: {
      type: 'string',
      required: true
    },
    url: {
      type: 'string'
    },
    group: {
      model: 'group'
    },
    users: {
      collection: 'user',
      via: 'notifications',
      dominant: true
    },
    read: {
      type: 'boolean',
      required: true,
      defaultsTo: false
    }
  },
  afterCreate: function (notification, cb) {

    //Add users

    if (!('users' in notification)) {
      notification.users = [];
    }
    if (notification.users.length <= 0) {

      Group.findOne(notification.group).populate('users').populate('notifications').exec(
        function (err, group) {
          if (err) {
            return sails.log('error populating the users in the notification afterCreate', err);
          }
          Notification.update({ id: notification.id }, { users: group.users }).exec(function (err, notif) {
            if (err) {
              return sails.log('error updating notigication', err);
            }
            cb();
          })


        }
      )
    } else {
      cb();
    }

  }
};

