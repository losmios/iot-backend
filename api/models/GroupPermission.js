/**
 * GroupPermission.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    user: {
      model: 'user',
    },
    group: {
      model: 'group'
    },
    permissions: {
      type: 'array',
      isPermission: true
    },
    status: {
      type: 'string',
      enum: ['active', 'blocked', 'joining'],
      defaultsTo: 'joining'
    }
  },
  beforeCreate: function (values, cb) {
    GroupPermission.find({ user: values.user, group: values.group }).exec(function (err, gp) {
      if (err) return cb(err);
      if (gp.length > 0) {
        return cb('Integrity error user cannot have more than one group permission object by group')
      } else {
        return cb();
      }
    })
  },
  types: {
    isPermission: function (values) {
      let permissionsList = GroupService.permissionsList;
      if (values === null) {
        return true
      }
      if (!_.isArray(values)) {
        return false;
      }
      for (let i = 0; i < values.length; i++) {
        let v = values[i];
        if (!_.isString(v)) {
          return false;
        }
        if (permissionsList.indexOf(v) === -1) {
          return false;
        }
      }
      return true;
    }
  }
};

