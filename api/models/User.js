/**
 * User
 *
 * @module      :: Model
 * @description :: This is the base user model
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = {

  attributes: require('waterlock').models.user.attributes({

    /* e.g.
    nickname: 'string'
    */
    groups: {
      collection: 'group',
      via: 'users',
      through: 'grouppermission'
    },
    firstName: {
      type: 'string'
    },
    lastName: {
      type: 'string'
    },
    notifications: {
      collection: 'notification',
      via: 'users'
    },
    devices: {
      collection: 'device',
      via: 'user'
    },
    isSubscribedToNotifications: {
      type: 'boolean',
      defaultsTo: false
    }

  }),

  toJSON: function () {
    var obj = this.toObject;
    delete obj.auth.password;
    return obj;
  },

  beforeCreate: require('waterlock').models.user.beforeCreate,
  beforeUpdate: require('waterlock').models.user.beforeUpdate
};
