/**
 * Group.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true,
      unique: true
    },
    users: {
      collection: 'user',
      via: 'groups',
      through: 'grouppermission'
    },
    things: {
      collection: 'thing',
      via: 'group'
    },
    notifications: {
      collection: 'notification',
      via: 'group'
    },
    incidents: {
      collection: 'incident',
      via: 'group'
    }
  }
};

