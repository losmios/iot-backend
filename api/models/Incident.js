/**
 * Incident.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    sensor: {
      model: 'sensor'
    },
    thing: {
      model: 'thing'
    },
    group: {
      model: 'group'
    },
    type: {
      type: 'string',
      enum: ['fail', 'start']
    }
  },
  afterCreate: function (values) {
    if (values.type === 'start') {
      if ('thing' in values && values.thing) {
        Thing.findOne(values.thing).exec(function (err, thing) {
          if (err) {
            sails.error(error);
          }
          if (!thing.alert && !thing.online) {
            thing.online = true;
            thing.save();
            IncidentService.checkOnlineTime({ thing: thing });
          }
        });
      }
      if (values.sensor) {
        IncidentService.checkOnlineTime({ sensor: values.sensor });
      }
    }
  }
};

