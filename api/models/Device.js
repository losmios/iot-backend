/**
 * Device.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    endpoint: {
      type: 'string',
      required: true,
      unique: true
    },
    lastUsed: {
      type: 'date'
    },
    active: {
      type: 'boolean',
      defaultsTo: true
    },
    user: {
      model: 'user',
      required: true
    },
    useEndpoint: function () {
      this.lastUsed = new Date();
      return this.endpoint;
    }
  }
};

