/**
 * Thing.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {


  attributes: {
    name: {
      type: 'string'
    },
    domain: {
      type: 'string',
      url: true
    },
    ip: {
      type: 'string',
      ip: true
    },
    mac: {
      type: 'string',
      //regex: "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$"
    },
    alert: {
      type: 'boolean',
      defaultsTo: false,
      required: true
    },
    sensors: {
      collection: 'sensor',
      via: 'thing'
    },
    group: {
      model: 'group',
      required: true
    },
    incidents: {
      collection: 'incident',
      via: 'thing'
    },
    timeout: {
      type: 'integer',
      defaultsTo: parseInt(1000 * 60 * 5)
    },
    refreshRate: {
      type: 'integer',
      defaultsTo: parseInt(1000 * 5)
    },
    online: {
      type: 'boolean',
      defaultsTo: true
    },
    onlineTime: {
      type: 'string',
      defaultsTo: '0'
    }
  },
  checkAlert: function (id, cb) {
    Thing.findOne(id).populate('sensors').exec(function (err, thing) {
      if (err) return cb(err)
      if (!thing) return cb(new Error('THing not found.'))
      var alert = false;
      thing.sensors.map(sensor => {
        if (sensor.alert === true) alert = true
      })
      if (thing.alert && !thing.sensors.filter(s => s.alert).length) {
        thing.alert = false;
        Incident.create({
          thing, thing,
          type: 'start'
        });
        thing.save();
        Thing.publishUpdate(thing.id, { alert: false });
      } else {
        return cb
      }
    })
  },
  afterDestroy: function (thing, cb) {
    Sensor.destroy({ thing: thing.id }).exec(cb);
  },
  afterUpdate: function (updatedRecord, cb) {

    Sensor.find({ thing: updatedRecord.id }).exec(function (err, sensors) {
      if (err) return cb(err);
      sensors.map(s => {
        s.timeout = updatedRecord.timeout;
        s.save();
      })
    })

  },
  afterCreate: function (values) {
    IncidentService.checkOnlineTime({ thing: values });
  }
};

