/**
 * ThingController
 *
 * @description :: Server-side logic for managing things
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    update: function (req, res) {
        let id = req.params.id || null;
        if (!id) {
            return res.badRequest('Thing id not provided')
        }
        if (!('body' in req)) {
            return res.badRequest('Body not provided so nothing to update');
        }
        Thing.findOne({ id: id }).exec(function (err, thing) {
            if (err) {
                res.serverError(err);
            }
            thing = Object.assign(thing, req.body);
            Thing.publishUpdate(thing.id, req.body, req);
            thing.save();
            return res.json(thing.toObject());

        });
    },
    lastIncident: function (req, res) {
        let id = req.params.id || null;
        if (!id) {
            return res.badRequest('Thing id not provided')
        }
        IncidentService.getLastIncident({ thing: id })
            .then(incident => {
                res.json(incident);
            })
            .catch(error => {
                res.serverError(error);
            })
    },
    hello: function (req, res) {
        return res.json({ message: 'Hello' });
    },
    create: function (req, res) {
        if (!req.body) {
            res.badRequest('No body provided for creating new Thing');
        }
        if (!('name' in req.body) || !_.isString(req.body.name)) {
            res.badRequest('Name is required and has to be a string');
        }
        if (('group' in req.body)) {
            var group = parseInt(req.body.group, 10);
            if (!_.isNumber(group)) {
                res.badRequest('Group id is required and has to be a number');
            }
        } else {
            res.badRequest('Group id is required and has to be a number');
        }
        Group.findOne(req.body.group).then(group => {
            if (!group) {
                res.badRequest('group doesnt exists');
            }
            Thing.create(req.body).then(thing => {
                sails.log.info('thing created', thing);
            }).catch(err => {
                sails.log.error(err);
                res.serverError(err);
            });
            return res.send('Thing created! I hope');
        }).catch(err => {
            return res.serverError(err);
        })
    }

};
