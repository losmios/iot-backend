/**
 * GroupController
 *
 * @description :: Server-side logic for managing groups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var permissionsList = [
        'admin', 
        'read', 
        'write', 
        'update_timeout', 
        'add_user', 
        'create_thing',
        'update_thing',
        'delete_thing',
        'create_sensor',
        'delete_sensor',
        'update_sensor',
        'create_measure',
        'update_measure',
        'delete_measure'
      ];

var validatePermissions = function(permissions){
	for(var i = 0; i < permissions.length; i++){
		if(permissionsList.indexOf(permissions[i]) === -1){
			return false;
		}
	}
	return true;
}

module.exports = {
	create: function (req, res) {
		if (!req.session.authenticated) {
			return res.forbidden('Not allowed');
		}
		if (!req.session.user) {
			return res.serverError('No user found');
		}
		if (req.method !== 'POST') {
			return res.send(405, 'method ' + req.method + ' not allowed')
		}
		if (!('name' in req.body)) {
			return res.badRequest('Missing argument: name');
		}
		Group.create({
			name: req.body.name,
		}).exec(function (err, group) {
			if (err) res.serverError(err);
			GroupPermission.create({
				user: req.session.user.id,
				group: group.id,
				permissions: ['admin', 'read'],
				status: 'active'
			}).exec(function (err, gp) {
				sails.log('groupPermission created', gp);
				if (err) return res.serverError(err);
			})
			return res.json(group);
		})

	},
	admins: function (req, res) {
		if (req.method !== 'GET') {
			return res.send(405, 'Method ' + req.method + " not allowed");
		}
		if (!req.params) {
			return res.send(404, 'Group not found');
		}
		var id = req.params.id;
		GroupPermission.find(
			{
				group: id,
				permissions: {
					contains: 'admin'
				}
			}).populate('user').exec(function (err, gp) {
				if(err) return res.serverError(err);
				var users = []
				gp.map(g => {
					users.push(g.user);
				})
				return res.json({
					admins: users
				})
			})
	},
	addUser: function(req, res){
		if(req.method !== 'POST'){
			return res.send(405, 'Method ' + req.method + " not allowed");
		}
		if (!req.params) {
			return res.send(404, 'Group not found');
		}
		var id = null;
		var permissions = [];
		if(!('user' in req.body)){
			return res.badRequest('User not provided')
		}
		if(!('id' in req.params)){
			return res.send(404, 'Group not found');
		}else{
			groupId = req.params.id;
		}
		if(!('permissions' in req.body)){
			permissions = ['read']
		}else{
			permissions = req.body.permissions;
			if(_.isString(permissions)){
				permissions = permissions.split(',');
			}
			if(!validatePermissions(permissions)){
				return res.badRequest({
					message: "Permissions invalid",
					validPermissions: permissionsList
				})
			}
		}
		var body = req.body;
		body['permissions'] = permissions;
		GroupPermission.find({group: groupId, user: body.user}).populate('group').populate('user').exec(function(err, gp){
			if(err) return res.serverError(err);
			if(gp.length > 1){
				return res.serverError('Integrity error user can only have one groupPermission object by group')
			}else if(gp.length === 1){
				gp = gp[0];
				permissions.map(p => {
					if(gp.permissions.indexOf(p) === -1){
						gp.permissions.push(p);
					}
				})
				gp.save();
				return res.json(gp);
			}else{
				GroupPermission.create({
					user: body.user, 
					group: groupId,
					permissions: body.permissions
			}).exec(function(err, gp){
				if(err) return res.serverError(err);
				Group.findOne(gp.group).exec(function(err, g){
					g.users.add(gp.user);
					g.save();
					return res.json(gp);
				})
			})
			}
		})
	},
	join: function(req, res){
		req.body['permissions'] = 'read';
		if(!('user' in req.body)){
			req.body['user'] = req.session.user.id;
		}
		if(req.method !== 'POST'){
			return res.send(405, 'Method ' + req.method + " not allowed");
		}
		if (!req.params) {
			return res.send(404, 'Group not found');
		}
		var id = null;
		var permissions = [];
		if(!('user' in req.body)){
			return res.badRequest('User not provided')
		}
		if(!('id' in req.params)){
			return res.send(404, 'Group not found');
		}else{
			groupId = req.params.id;
		}
		GroupService.join({
			user: req.body.user,
			group: groupId
		}).then(function(gp){
			return res.json(gp);
		})
	},
	members: function(req, res){
		if(!( 'id' in req.params)){
			return res.badRequest('Group not found');
		}
		GroupService.getMembers(req.params.id)
			.then(members => {
				res.json(members);
			})
			.catch(err => {
				res.serverError(err);
			})
	},
	leave: function(req, res){
		if(!('id' in req.params) || !('uid' in req.params)){
			return res.badRequest('Group id and User id must be provided to leave group');
		}
		GroupService.leave({
			user: req.params.uid,
			group:req.params.id
		}).then(function(gp){
			return res.json({message: 'User has leaved the group'});
		}).catch(function(err){
			return res.serverError(err)
		})
	}
};

