
function validateGroupUser(options) {
	if (!('group') in options) {
		return 'Group not provided'
	}
	if (!('user') in options) {
		return 'User not provided'
	}
}
module.exports = {
	permissionsList: [
		'admin',
		'read',
		'write',
		'update_timeout',
		'add_user',
		'create_thing',
		'update_thing',
		'delete_thing',
		'create_sensor',
		'delete_sensor',
		'update_sensor',
		'create_measure',
		'update_measure',
		'delete_measure'
	],
	validatePermissions: function (permissions) {
		for (var i = 0; i < permissions.length; i++) {
			if (permissionsList.indexOf(permissions[i]) === -1) {
				return false;
			}
		}
		return true;
	},
	getMembers: function (id) {
		return new Promise(function (resolve, reject) {
			if (!id) {
				return reject('id Not Provided');
			}
			var members = [];
			GroupPermission.find({ group: id }).exec(function (err, gps) {
				if (err) return reject(err);
				var counter = gps.length - 1;
				gps.map(gp => {
					User.findOne(gp.user).populate('auth').exec(function (err, user) {
						user.permissions = gp.permissions;
						user['status'] = gp.status;
						members.push(user);
						if (counter <= 0) {
							resolve(members);
						} else {
							counter--;
						}
					})
				})
			})
		})
	},
	join: function (options) {
		return new Promise(function (resolve, reject) {
			var error = validateGroupUser(options);
			if (error) {
				return reject(error);
			}
			var group = options.group.id || options.group;
			var user = options.user.id || options.user;
			GroupPermission.find({ group: group, user: user }).populate('group').populate('user').exec(function (err, gp) {
				if (err) return reject(err);
				if (gp.length > 1) {
					return reject('Integrity error user can only have one groupPermission object by group')
				} else if (gp.length === 1) {
					return resolve('User already joined');
				} else {
					GroupPermission.create({
						user: user,
						group: group,
						permissions: null,
						status: 'joining'
					}).exec(function (err, grouppermission) {
						if (err) return reject(err);
						Group.findOne(grouppermission.group).exec(function (err, g) {
							if (err) return reject(err);
							g.users.add(grouppermission.user);
							//g.save();
							return resolve(grouppermission);
						})
					})
				}
			})
		});
	},
	isGroupAdmin: function (options) {
		return new Promise(function (resolve, reject) {
			if (validateGroupUser(options)) {
				reject('Please provide user and group ids');
			}
			var user = options.user['id'] || options.user;
			var group = options.group['id'] || options.group;
			GroupPermission.findOne({ user: user, group: group }).exec(function (err, groupPermission) {
				if (err) {
					reject(err);
				}
				if (!err && groupPermission && groupPermission.permissions && groupPermission.permissions.indexOf('admin') !== -1) {
					return resolve(true);
				} else {
					// User is not allowed
					// (default res.forbidden() behavior can be overridden in `config/403.js`)
					return resolve(false);
				}
			});
		})

	},
	leave: function (options) {
		return new Promise(function (resolve, reject) {
			var error = validateGroupUser(options);
			if (error) {
				return reject('Error please provide Group ID and User ID');
			}
			GroupPermission.destroy({ group: options.group, user: options.user })
				.exec(function (err) {
					if (err) return reject(err);
					Group.findOne(options.group).exec(function (err, group) {
						if (err) return reject(err);
						group.users.remove(options.user);
						group.save();
						return resolve();
					})
				})
		})
	}

}