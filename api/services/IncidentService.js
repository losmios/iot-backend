
var incidentBuffer = [];
var countingBuffer = [];

var addTime = function (obj) {
	if (!obj) {
		sails.log.error('obj not found with obj id ', obj);
	}
	if (!obj.alert) {
		var onlineTimeString = parseInt(obj.onlineTime);
		obj.onlineTime = (onlineTimeString + 5) + "";
		obj.save();
	}
	return obj;
};

module.exports = {
	getLastIncident(options) {
		return new Promise((resolve, reject) => {
			var thing = options.thing || null;
			var sensor = options.sensor || null;
			var group = options.group || null;
			if (!thing && !group && !sensor) {
				reject('Nor thing, group or sensor provided');
			}
			var query = {
				where: {
					thing: thing,
					sensor: sensor,
					group: group,
					type: 'fail'
				},
				limit: 1,
				sort: 'createdAt DESC'
			}
			Incident.find().exec(function (err, incident) {
				if (err) {
					reject(err);
				}
				resolve(incident.createdAt);
			});
		});
	},
	checkOnlineTime: function (options) {
		var thing = null;
		var sensor = null;
		if ('thing' in options && options.thing) {
			if (_.isNumber(options.thing)) {
				thing = options.thing;
			} else {
				if ('id' in options.thing) {
					thing = options.thing.id;
				} else {
					sails.log.error('No thing id provided for checkOnlineTime');
				}
			}
		}
		if ('sensor' in options && options.sensor) {
			if (_.isNumber(options.sensor)) {
				sensor = options.sensor;
			} else {
				if ('id' in options.sensor) {
					sensor = options.sensor.id;
				} else {
					sails.log.error('No sensor id provided for checkOnlineTime');
				}
			}
		}
		if (!thing && !sensor) {
			sails.log.error('Nor Thing or sensor id provided for checkOnlineTime');
		}
		if (thing) {
			Thing.findOne(thing).exec(function (err, thing) {
				if (err) {
					sails.log.error(err);
				}
				addTime(thing);
				setTimeout(IncidentService.checkOnlineTime.bind(null, { thing: thing }), 5000);
			});
		}
		if (sensor) {
			Sensor.findOne(sensor).exec(function (err, sensor) {
				if (err) {
					sails.log.error(err);
				}
				addTime(sensor);
				setTimeout(IncidentService.checkOnlineTime.bind(null, { sensor: sensor }), 5000);
			})
		}


	},
	onlineTime: function (options) {
		return new Promise((resolve, reject) => {
			var thing = options.thing || null;
			var sensor = options.sensor || null;
			if (!thing && !sensor) {
				reject('Nor thing or sensor provided for calculating the online time');
			}
			if (thing) {
				Thing.findOne({ thing: thing }).exec(function (err, values) {
					if (err) {
						sails.log.error(err);
						reject(err);
					}
				})
				var query = {
					where: {
						thing: thing,
						type: 'fail'
					},
					limit: 1,

				}
				Incident.find({ thing: thing })
			}
			if (sensor) {
				reject('Function not implemented yet');
			}
		});

	}
}