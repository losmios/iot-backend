
let timeoutHandle = function (sensor) {
  sails.log('entering Timeout')
  let timeAgo = new Date();
  timeAgo.setTime(timeAgo.getTime() - sensor.timeout);
  Sensor.findOne({ id: sensor.id })
    .exec(function (error, sensor) {
      if (error) {
        return sails.log(error);
      }
      lastMeasureTime = new Date(sensor.lastMeasure.createdAt);
      //sails.log('currentTime = ' + new Date().toLocaleTimeString());
      //sails.log('timeout = ' + timeAgo.toLocaleTimeString());
      //sails.log('LastMeasure = ' + lastMeasureTime.toLocaleTimeString());
      //sails.log('timeout = ' + timeAgo.getTime());
      //sails.log('LastMeasure = ' + lastMeasureTime.getTime());
      if (lastMeasureTime.getTime() < timeAgo.getTime()) {
        sensor.alert = true;
        sails.log('ALERT on sensor ' + sensor.name + " id: " + sensor.id);
        //Broadcast Message Alert
        Sensor.publishUpdate(sensor.id, { alert: true });
        Incident.create({
          sensor: sensor.id,
          thing: sensor.thing,
          type: 'fail'
        }).exec(function (err, values) {
          if (err) {
            sails.error(err);
          }
          sails.log('Incident created');
        });
        sensor.sendAlert();
        sensor.save();
      }
    })
}

var alerts = [];

module.exports = {


  registerTimeout: function (options, done) {
    if (!('sensor' in options)) {
      return done('please give me a sensor')
    }

    alert = alerts.find(f => f.sensorId === options.sensor.id);
    if (!alert) {
      t = setTimeout(timeoutHandle.bind(null, options.sensor), options.sensor.timeout);
      alerts.push({
        sensorId: options.sensor.id,
        timeout: t
      })
    } else {
      clearTimeout(alert.timeout);
      alert.timeout = setTimeout(timeoutHandle.bind(null, options.sensor), options.sensor.timeout);
    }

  }

}