
var request = require('request');

function getDevicesByGroup(group) {
	var promise = new Promise((resolve, reject) => {
		let devices = []
		Group.findOne(group)
			.populate('users')
			.exec((err, group) => {
				if (err) {
					return reject(err);
				}
				//Verify if the devices populates
				let counter = group.users.length;
				group.users.map(user => {

					getDevicesByUser(user).then(d => {
						d.map(dev => {
							devices.push(dev);
						})
						counter--;
						if (counter <= 0) {
							resolve(devices);
						}
					}).catch(error => {
						sails.log(error);
					})
					
					
				})
			})
	})

	return promise;

}

function getDevicesByUser(user) {
	if(!_.isNumber(user)){
		user = user.id;
	}
	var promise = new Promise((resolve, reject) => {
		User.findOne(user).populate('devices').exec((err, user) => {
			if (err) {
				sails.log(err);
				return reject(err);
			}
			resolve(user.devices);

		});
	})

	return promise;

}

const FCM_KEY = "AIzaSyAA1YoWqbvSpSUuYzZWX0oVWBwy6zJK1Ps";
const FCM_HEADERS = {
	'Authorization': 'key='+FCM_KEY,
	'Content-Type': 'application/json'
}
const FCM_HOST = "https://fcm.googleapis.com";
const FCM_PATH = "/fcm/send"
const FCM_URL = "https://fcm.googleapis.com/fcm/send";

module.exports = {

	/**
	 * Send push notification to the fcm to handle 
	 * @required {String} title
	 * 	The title of the notification to display
	 * @required {Array<User | number>} users
	 * 	The users to send the push
	 * @required {}
	 */
	sendNotification: function (options, done) {
		if (!('group' in options) && !('users' in options)) {
			return done('No groups nor users to send this to, if you have many cats you shuld try tinder')
		}
		let groups = null
		let devices = []
		if ('group' in options) {
			group = options.group;
		}
		let users = []
		if ('users' in options) {
			if(options.users.length > 0){
				users = options.users;
			}
		}
		if (group) {
			if (!_.isArray(group)) {
				if (!_.isString(group)) {
					if (!_.isNumber(group)) {
						return done('group parameter invalid');
					}
					getDevicesByGroup(group).then(
						dev => {
							dev.map(d => {
								devices.push(d);
							})
							var endpoints = [];
							devices.map(device => {
								let registration_id = device.useEndpoint();
								endpoints.push(registration_id);
							})	

							request.post({
									url: FCM_URL,
									headers: FCM_HEADERS,
									body: {registration_ids: endpoints},
									json: true
								},
								function(err, res, body){
									/*sails.log(err);
									sails.log(res);
									sails.log(body);*/
								})
						}
					)
				}
			} 
		}
		if (users.length > 0) {
			users.map(user => {
				getDevicesByUser(user)
					.then(devs => {
						devs.map(d => {
							devices.push(d);							
						})
					})
			})
		}
		



	}
}