/**
 * isAdmin
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any admin user
 *                 Assumes you are admin of the group the thing is part of
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function (req, res, next) {

	// User is allowed, proceed to the next policy, 
	// or if this is the last policy, the controller
	let user = req.session.user;
	if (!user) {
		return res.forbidden('User not Authenticated');
	}
	let route = req.route.path;
	if (route.startsWith('/thing')) {
		Thing.findOne({ id: req.params.id }).exec(function (err, thing) {
			let group = null;
			if (_.isNumber(thing.group)) {
				group = thing.group;
			} else {
				group = thing.group.id;
			}
			GroupService.isGroupAdmin({
				user: user.id,
				group: group
			}).then(function (isAdmin) {
				if (isAdmin) {
					return next();
				} else {
					return res.forbidden('Only admin users are allowed');
				}
			}).catch(function (err) {
				return res.serverError(err);
			});
		});
	} else if (route.startsWith('/group')) {
		GroupPermission.findOne({ user: user.id, group: req.params.id }).exec(function (err, groupPermission) {
			if (!err && groupPermission && groupPermission.permissions && groupPermission.permissions.indexOf('admin') !== -1) {
				return next();
			} else {
				// User is not allowed
				// (default res.forbidden() behavior can be overridden in `config/403.js`)
				return res.forbidden('Only Admin users are allowed');
			}
		});
	}




};
