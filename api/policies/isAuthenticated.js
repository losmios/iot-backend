var passport = require('passport');

module.exports = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    } else {
        //console.log(req);
        if (req.headers['authorization']) {
            console.log(req.headers['authorization']);
            passport.authenticate('basic', { session: false }, function (err, user, info) {
                if ((err) || (!user)) {
                    if ('application/json' === req.headers["content-type"]) {
                        return res.status(403).json({
                            message: 'User not Authenticated'
                        });
                    }
                    return res.redirect('/login');
                    /*
                    return res.send({
                        message: info.message,
                        user: user
                    });*/
                }
                return next();
            })(req, res, next);
        }
        
    }
}